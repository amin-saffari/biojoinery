#!/bin/sh

# Inits project from configure.ac
case `uname` in Darwin*) glibtoolize --copy ;;
	*) libtoolize --copy ;; esac

aclocal && autoheader && automake --add-missing && autoconf
