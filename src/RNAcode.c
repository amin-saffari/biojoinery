/*  Copyright 2009, Stefan Washietl

    This file is part of RNAcode.

    RNAcode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RNAcode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RNAcode.  If not, see <http://www.gnu.org/licenses/>. */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include "RNAcode.h"
#include "rnaz_utils.h"
//#include "treeML.h"
#include "cmdline.h"
#include "snugFit.h"
#include "trees.h"
//Only because of ntMap
#include "score.h"
//
#include "joinery_misc.h"

//#include "interface.h"

/* TODO: All phylofit library except "msa.h" and "phylo_fit.h" must be relocated to maf.c*/
// phylofit part
#include "msa.h"
#include "gff.h"
#include "category_map.h"
#include "tree_model.h"
#include "fit_em.h"
#include "subst_mods.h"
#include "misc.h"
#include "local_alignment.h"
#include "ctype.h"
#include "tree_likelihoods.h"
#include "numerical_opt.h"
#include "sufficient_stats.h"
#include "phylo_fit.h"
#include "maf.h"
#include "maf_block.h"

/* Global variables shared troughout the application */

parameters pars; /* user options */
bgModel *models, *modelsRev; /* Background model data for current alignment*/
float**** Sk;  /* Main score matrix which is allocated only once and re-used*/

/* Save matrix for native alignment for backtracking afterwards */ 
float**** Sk_native;   
float**** Sk_native_rev;  

long int hitCounter;

void usage(void){
  help();
}

void help(void){

  cmdline_parser_print_help();
  printf("\n\n");
}

void version(void){
  cmdline_parser_print_version ();
  /*printf("RNAcode v " PACKAGE_VERSION " Wed Jan  5 13:14:57 EST 2011\n");
  exit(EXIT_SUCCESS);*/
}

void read_commandline(int argc, char *argv[], struct phyloFit_struct *pf){

  int x;

  struct gengetopt_args_info args;

  if (cmdline_parser (argc, argv, &args) != 0){
    //usage();
    exit(EXIT_FAILURE);
  }
  
  if (args.inputs_num >= 1){
    pars.inputFile = phast_fopen(args.inputs[0], "r"); 
    if (pars.inputFile == NULL){
      fprintf(stderr, "ERROR: Can't open input file %s\n", args.inputs[0]);
      exit(EXIT_FAILURE);
    }
    strcpy(pars.inputFileName,args.inputs[0]);
  }
  
  if (args.outfile_given){
    pars.outputFile = phast_fopen(args.outfile_arg, "w");
    if (pars.outputFile == NULL){
      fprintf(stderr, "ERROR: Can't open output file %s\n", args.outfile_arg);
      exit(EXIT_FAILURE);
    }
  }


  if (args.num_samples_given){
    pars.sampleN=args.num_samples_arg;
  }

  if (args.blosum_given){
    if (args.blosum_arg !=62 && args.blosum_arg !=90){
      fprintf(stderr, "ERROR: Currently only BLOSUM62 and BLOSUM90 are supported.\n");
      exit(EXIT_FAILURE);
    } else {
      pars.blosum=args.blosum_arg;
    }
  }

  if (args.gtf_given){
    pars.outputFormat=1;
  }

  if (args.tabular_given){
    pars.outputFormat=2;
  }

  if (args.pars_given){

    x=sscanf(args.pars_arg,"%f,%f,%f,%f",&pars.Delta,&pars.Omega,&pars.omega, &pars.stopPenalty_0);

    if (!x){
      fprintf(stderr, "ERROR: Format error in parameter string. Refer to README how to use --pars.\n");
      exit(EXIT_FAILURE);
    }
  }
  
  if (args.best_only_given){
    pars.bestOnly=1;
  }

  if (args.best_region_given){
    pars.bestRegion=1;
  }

  if (args.stop_early_given){
    pars.stopEarly=1;    
  }

  if (args.cutoff_given){
    pars.cutoff=args.cutoff_arg;
  }

  if (args.eps_cutoff_given){
    pars.postscript_cutoff = args.eps_cutoff_arg;
  }

  if (args.eps_given){
    pars.postscript = 1;
  }

  if (args.eps_dir_given){
    strcpy(pars.postscriptDir, args.eps_dir_arg);
  } 

  if (args.coordinate_given){
    strcpy(pars.gffFile, args.coordinate_arg);
    if (pars.gffFile == NULL){
      fprintf(stderr, "ERROR: Can't open coordinate file %s\n", args.coordinate_given);
      exit(EXIT_FAILURE);
    }
  } 

  if (args.treeFile_given){
    strcpy(pars.treeFile, args.treeFile_arg);
    if (pars.treeFile == NULL){
      fprintf(stderr, "ERROR: Can't open tree file %s\n", args.treeFile_given);
      exit(EXIT_FAILURE);
    }
  } 

  if (args.debug_file_given){
    strcpy(pars.debugFileName,args.debug_file_arg);
  }

  if (args.limit_given){
    strcpy(pars.limit,args.limit_arg);
  }
  
  if (args.fourfold_given){
    pars.fourD = 1;
  }

  if(args.misToINDEL_given){
    pars.missing_as_indels = 1;
  }

  if (args.nrates_given){
    pf->nratecats = args.nrates_arg;
  }
  
  if (args.EM_given){
    pf->use_em = 1;
    //Precision (MED) is combined with EM option and can't be chang.
    pf->precision = OPT_MED_PREC;
  }

  if (args.verbose_given){
      pars.quiet = 0;
      pf->quiet = pars.quiet;
  }

  if (args.subst_mod_given){
      //JC69|F81|HKY85|HKY85+Gap|REV|SSREV|UNREST|R2|R2S|U2|U2S|R3|R3S|U3|U3S
      //And others!!! Look at the function below
      pf->subst_mod = tm_get_subst_mod_type(args.subst_mod_arg);
      if (pf->subst_mod  == UNDEF_MOD){
          fprintf(stderr, "ERROR: Not a valid substitution model.\n");
          exit(EXIT_FAILURE);
      }
  }

  if (args.minInfoSite_given){
      pf->nsites_threshold = args.minInfoSite_arg ; //get_arg_int(arg); // min-informative default is 50.
  }
  
  if (args.transcriptTag_given){
    strcpy(pars.transcriptgroup_tag, args.transcriptTag_arg);
  } 
  
  if (args.exonTag_given){
    strcpy(pars.exongroup_tag, args.exonTag_arg);
  } 

  if (args.help_given){
      cmdline_parser_print_help();
      printf("\n\n");
    //help();
    exit(EXIT_SUCCESS);
  }

  if (args.version_given){
      cmdline_parser_print_version ();
    //version();
    exit(EXIT_SUCCESS);
  }

  cmdline_parser_free(&args);

}

void setDefault(struct phyloFit_struct* pf){
  // RNAcode parameters 
  pars.Delta=-10.0;
  pars.Omega=-4.0;
  pars.omega=-2.0;
  pars.stopPenalty_k=-8.0;
  pars.stopPenalty_0=-9999.0;
  pars.inputFile=stdin;
  pars.outputFile=stdout;
  pars.debugFile=stderr;
  
  pars.bestOnly=0;
  pars.bestRegion=0;
  pars.stopEarly=0;
  pars.postscript=0;
  pars.postscript_cutoff=0.05;
  strcpy(pars.postscriptDir, "eps");
  pars.sampleN=100;
  pars.blosum=62;
  strcpy(pars.limit,"");
  pars.cutoff=1.0;
  pars.outputFormat=0; /* 0: normal list; 1: GTF; 2:compact list (debugging) */
  strcpy(pars.debugFileName,"");
  strcpy(pars.inputFileName,"STDIN");
  pars.quiet = 1;

  strcpy(pars.alphabet,"ACGTNacgtn"); 
  strcpy(pars.gffFile,"tmp.gff");
  strcpy(pars.treeFile,"tmp.nh");
  pf->nratecats = 1;
  pf->use_em = 0;//FALSE
  pf->quiet = pars.quiet;
  pf->subst_mod= tm_get_subst_mod_type("HKY85");//JC69|F81|HKY85|HKY85+Gap|REV|SSREV|UNREST|R2|R2S|U2|U2S|R3|R3S|U3|U3S
  pf->nsites_threshold = 1 ; //min-informative default is 50.
  //Different isoforms of a same gene are grouped based on their transcript-name and exon-name .
  //Naming is based on refeature utility which is part of PHAST package.
  strcpy(pars.transcriptgroup_tag, "transcript_id");
  strcpy(pars.exongroup_tag, "exon_id");
  pars.fourD = 0; 
  pars.missing_as_indels = 0;
  pars.windowSize = 10000;

}

int main(int argc, char *argv[]){

  double kappa, maxScore;
  clock_t startTime;	
  float runtime;

  struct aln *inputAln[MAX_NUM_NAMES];
  struct aln *inputAlnRev[MAX_NUM_NAMES];
  
  struct phyloFit_struct *pf =  phyloFit_struct_new(1); 
  

  setDefault(pf);
  read_commandline(argc, argv, pf);

  msa_format_type output_format = MAF;
  GFF_Set *gff=NULL, *liftedGFF=NULL , *gffSub ;
  String *refseq = NULL, *currRefseq;
  FILE *TMPoutfile=NULL;
  char *reverse_group = NULL; // Probably nowhere
  List *orderSeqs = NULL; //TODO : Convert it to be a parameter specific
  List *includeSeqs = NULL; //TODO : Convert it to be a parameter specific
  

  pf->gff = NULL;
  pf->output_fname_root = NULL;
  
  srand(time(NULL));

  //TODO: This has a conflict with the whole purpose of using phast alphabet
  ntMap['A']=ntMap['a']=0;
  ntMap['C']=ntMap['c']=1;
  ntMap['G']=ntMap['g']=2;
  ntMap['T']=ntMap['t']=3;
  ntMap['U']=ntMap['u']=3;

  startTime=clock();
  
  //Parameters mostly addressing MAF handling
  int seq_keep =TRUE;
  int store_order= TRUE;


  //TODO: Given phastCons.c:319 we can handle multiple tree modles. This can be
  //helpful if we want to check multiple species tree at once (i.e because of
  //ILS). Right now we support only one spicies tree at a time!!
  //Although we have the opportuniy, this is expensive. Fitting the Vertebrate
  //tree is much more slower than primates. This means we are depending the
  //result of primate to vertebrate! (which doesn't sound wise)
  /*fprintf(stdout, "Read & set all species tree\n");
  List *tree_fname_list;
  tree_fname_list = get_arg_list(pars.treeFile); 
  numTrees=lst_size(mod_fname_list);
  p->mod = (TreeModel**)smalloc(sizeof(TreeModel*) * p->nummod);

  for (i = 0; i < p->nummod; i++) {
      String *fname = lst_get_ptr(mod_fname_list, i);
      if (p->results_f != NULL)
          fprintf(p->results_f, "Reading tree model from %s...\n", fname->chars);
      p->mod[i] = tm_new_from_file(phast_fopen(fname->chars, "r"), 1);
  }*/

  FILE *treeFile = phast_fopen(pars.treeFile, "r");
  TreeNode *specTree = tr_new_from_file(treeFile);
  pf->tree = specTree; //tr_new_from_file(treeFile);
  phast_fclose(treeFile);
  
  /* read alignment */
  // We read block by block and if there is an overlapp between our block and
  // gff then we go for other calculations, otherwise keep reading. This strategy
  // must be super expensive in the case of isoform reading as it's goin to read
  // the file multiple times. However, in that case we can read everything once
  // and then switch to the other calculations.

  // Parameter space : (input, Refseq, tuple_size, 
  //                    alphabet,gff, CategoryMap(cm), 
  //                    cycle_size, store_order, reverse_groups
  //                    gap_strip_mode, keep_overlapping, cats_to_do, 
  //                    seqnames, seq_keep)
  // TODO: Regarding tuple_size: Supporting dinucleotid model can make a lot of
  // thing more complicated that they should be. Also dinucleotid in DNA level
  // doesn't have the same meaning as dinucleotid substitution model on RNA 
  // level (i.e on structure).
  //
  // TODO: Regarding cycle_size: This is also depending upon tuple_size (i.e
  // supporting nonoverlapping or not)
  //
  //Regarding store_order it must be TRUE always.
  // This implemantation is not inspired (it is somehow copied) from maf_read_cats_subset.
  //MSA * tiny_MSA = maf_read_cats_subset(pars.inputFile, NULL, 1, 
  //        NULL, gff , cm, 
  //        -1 , TRUE, NULL, 
  //        NO_STRIP, FALSE, NULL, 
  //        orderSeqs, 1);

  //TODO: Most be an input parameter
  //Who is the reference.  Index of reference sequence for coordinates.  Use 0
  //to indicate the coordinate system of the alignment as a whole
  //Specified GFF is in the coordinate of this reference!
  int refseqIndex = 1 ;
  int gap_strip_mode = NO_STRIP;
  int tuple_size = 1;

  int label_categories = TRUE;  //if false, assume MSA already has categories labelled with correct gff

  List *split_indices_list = NULL, *segment_ends_list = NULL;
  MSA *mainMSA = NULL;
  msa_coord_map *map = NULL;
  //TODO: if input parameter is null then include only those species which are in your tree.
  //TODO: Do I need this?!
  if(includeSeqs==NULL){
      includeSeqs = NULL; //tr_leaf_names(pf->tree);
  } else { // Consider these species
      includeSeqs = NULL;
      //This must be somewhere else
  }

  // Plan is reading the whole MAF in memory and then chop it into the regions
  // which we like to work with.
  fprintf(stdout, "reading MAF file\n");
  mainMSA = maf_read_cats_subset(pars.inputFile, NULL, tuple_size, NULL, NULL /*gff*/ , NULL/*cm*/, -1 /*cycle_size*/, store_order, reverse_group /*NULL*/, NO_STRIP, FALSE, NULL/*cats_to_do*/, includeSeqs, 0/*seq_keep*/);
 
  //TODO: convert to be a parameter specific
  // 4d or Variable Rate over sites
  String *fourD_refseq=NULL;
  List *cats_to_do = NULL;
  CategoryMap *cm = NULL;
  char *reverse_groups_tag = NULL;


  //TODO: What if we don't have any region of interest!!
  if(pars.gffFile != NULL){
      fprintf(stdout, "reading regions\n");
      FILE *gffSets = phast_fopen(pars.gffFile, "r");
      gff = gff_read_set(gffSets);
      phast_fclose(gffSets);
      liftedGFF = gff_copy_set_no_groups(gff);
  }

  //TODO: Right now, we don't specify which category we like to work with. 
  if (gff == NULL && cm != NULL){
      fprintf(stderr, "Error: Can't map any category without gff.\n");
      exit(EXIT_FAILURE);
  }
  if (gff != NULL && cm == NULL){
      if (pars.fourD){
          cm = cm_new_string_or_file("NCATS=6; CDSplus 1-3; CDSminus 4-6");
          cats_to_do = lst_new_int(6); // These are all the categories but I only want to do the 3rd positions right now.
          // if you plan to do so be carful with the lst_free inthe end of the function
          for (int i=1; i<=6; i++) lst_push_int(cats_to_do, i);
          reverse_groups_tag = "transcript_id";
      }
      //TODO: DO I need this?
      //cm = cm_new_from_features(gff);
      if(pars.transcriptgroup_tag!=NULL){
          //fprintf(stdout, "switch to %s\n", (pars.fourD ? "fourD" :"variable rates"));
          // means you wanna work with 4d sites
          for (int i=0; i<lst_size(liftedGFF->features); i++) {
              GFF_Feature *f = lst_get_ptr(liftedGFF->features, i);
              if (f->frame == GFF_NULL_FRAME) f->frame = 0;
              if (fourD_refseq == NULL) fourD_refseq = str_new_charstr(f->seqname->chars);
              else if (!str_equals(fourD_refseq, f->seqname))
                  die("--4d requires all features are on the same chromosome");
              if (str_equals_charstr(f->feature, "CDS") && f->strand != '-')
                  str_cpy_charstr(f->feature, "CDSplus");
              else if (str_equals_charstr(f->feature, "CDS") && f->strand == '-')
                  str_cpy_charstr(f->feature, "CDSminus");
          }
          gff_group(gff,pars.transcriptgroup_tag);
          //gff_group(liftedGFF,pars.transcriptgroup_tag);
          if(pars.exongroup_tag!=NULL){ // group by exons of each transcript
              gff_exon_group(gff, pars.exongroup_tag); // if you don't have the exon feature it will add a dummy variable
              //gff_exon_group(liftedGFF, pars.exongroup_tag); // if you don't have the exon feature it will add a dummy variable
          }
          gff_group(gff,pars.transcriptgroup_tag); // back into the transcript grouping // TODO: I am not sure if I need it!?
          //gff_group(liftedGFF,pars.transcriptgroup_tag); // back into the transcript grouping // TODO: I am not sure if I need it!?
          if(!pars.quiet){
              fprintf(stderr,"GFF file grouped by %s tag!\n",pars.transcriptgroup_tag);
          }
      } else {
          fprintf(stderr,"No grouping!\n");
      }
      gff_sort(gff);
      //gff_sort(liftedGFF);
#ifdef DEBUG
      char *groupedGFFfile = malloc(strlen(pars.gffFile) + strlen(".grouped.sort.gff") + 1);
      strcpy(groupedGFFfile, pars.gffFile);
      strcat(groupedGFFfile, ".grouped.sort.gff");
      FILE *gffSets = phast_fopen(groupedGFFfile, "w");
      gff_print_set(gffSets, gff);
      phast_fclose(gffSets);
      fprintf(stderr, "sorted gff stored in %s file.\n",groupedGFFfile);
      free(groupedGFFfile);
#endif

  }

  if (refseqIndex < 0 || refseqIndex >= mainMSA->nseqs){
      fprintf(stderr, "Error: reference index is not valid!.\n");
      exit(EXIT_FAILURE);
  }
  //Using refseqIndex!=1 you can change your reference!
  //New value will change the coordinate of your alignment. Projection of the
  //previous MSA will do the job. This map will project everything into
  //the refseqIndex=0
  if(refseqIndex != 0){
      map = lift_coordinate(mainMSA, liftedGFF, refseqIndex);
  }


  if (orderSeqs != NULL)
    msa_reorder_rows(mainMSA, orderSeqs);

  if(gff != NULL){
      fprintf(stdout, "%li regions grouped into %li groups\n",lst_size(gff->features), lst_size(gff->groups));
      fprintf(stdout, "fitting tree based on %s\n",(pars.fourD ? "fourD sites" : "variable rates sites" ));
      if (mainMSA->ss->tuple_idx == NULL) {
          fprintf(stderr,"ERROR: ordered representation of alignment required with --gff.");
          exit(EXIT_FAILURE);
      }
      if (mainMSA->ss != NULL) ss_free_categories(mainMSA->ss);
      for(int i=0; i<lst_size(gff->groups);i++){
          char *treeString= NULL;
          /*seqs, names, nseqs, len, alphabet*/
          MSA *foldedMSA = msa_new(NULL, NULL, 0, 0, NULL);
          
          MSA *OrgfoldedMSA = NULL;

          GFF_FeatureGroup *f  = lst_get_ptr(gff->groups, i);
          GFF_FeatureGroup *lifted_f  = lst_get_ptr(liftedGFF->groups, i);


          fprintf(stderr, "Working on %s (%.1f%% completed)\n", f->name->chars,(100.0*(i+1))/(lst_size(gff->groups)));
          //Normal gff_subset_range(gff, f->start, f->end, FALSE); does not work
          //since we want to have a subset over type and not range

          GFF_Set *subgff = gff_new_set();
          str_cpy(subgff->gff_version, gff->gff_version);
          str_cpy(subgff->source, gff->source);
          str_cpy(subgff->source_version, gff->source_version);
          str_cpy(subgff->date, gff->date);

          /* This loop doesn't have amazing practical advantages over 
           * msa_split_by_gff. Two reasons lead into the reinvention, have a 
           * better debuging information and fill subgff to label categories 
           * of foldedMSA!
           * (look at msa_label_categories(foldedMSA,subgff,cm) and 
           * msa_split_by_gff(mainMSA, subgff));
           */
          int prev_exon_end = 0;
          for(int j=0; j<lst_size(lifted_f->features);j++){
              MSA *sub_msa = NULL;
              GFF_Feature *lifted_exon = lst_get_ptr(lifted_f->features, j);
              GFF_Feature *exon = lst_get_ptr(f->features, j);

              int from= /*(map != NULL) ? msa_map_msa_to_seq(map, lifted_exon->start): */ lifted_exon->start;
              int to = /*(map != NULL) ? msa_map_msa_to_seq(map, lifted_exon->end): */ lifted_exon->end;

#ifdef DEBUG
              fprintf(stderr, "from = %d and mapped from = %d\n",lifted_exon->start,msa_map_msa_to_seq(map, exon->start));
              fprintf(stderr, "to = %d and mapped to = %d\n",lifted_exon->end,msa_map_msa_to_seq(map, exon->end));
#endif
              //msa_split.c:765
              if (map != NULL){
                  if (msa_get_char(mainMSA, refseqIndex-1, lifted_exon->start -1) == GAP_CHAR) {
                      if (from == -1) from = 1;
                      else from++;
                  } else {
                      if (from == -1) from = 1;
                  }
              }
              if (j==0) {
                  foldedMSA->idx_offset= exon->start -1 ; //mainMSA->idx_offset + lifted_exon->start; //((map != NULL) ? msa_map_msa_to_seq(map, lifted_exon->start):  lifted_exon->start) -1 ; //((map != NULL) ? from : lifted_exon->start) -1 ; //mainMSA->idx_offset + (map == NULL ? exon->start : msa_map_msa_to_seq(map, exon->start))- 1;
              }

              /*
               * GFF and MAF don't have the same coordinate system. In GFF
               * coordinates are close (i.e. [a,b]) while in MAF they are half
               * close (i.e. [a,b)). To pretend we are doing something 
               * complicated "a" is 0-based in MAF while GFF is 1-base.
               * Here I do tranform 1-based closed coords to the zero-based, 
               * half-closed.
               */
              GFF_Feature *newfeat = gff_new_feature_copy(lifted_exon);
              /* convert subgff into the coordinate frame of the foldedMSA */
              newfeat->start = prev_exon_end + 1;
              newfeat->end   = to - (from -1) + prev_exon_end ;
              lst_push_ptr(subgff->features, newfeat);
              prev_exon_end  = newfeat->end;



              // TODO: What????
              //This is just for testing why I can't produce 4d otherwise I
              //don't need it. In real life I should set includeSeqs as NULL and
              //excludeseq as 0;

              sub_msa = msa_sub_alignment(mainMSA, NULL /*theseSeqs indices of the seq you want to include*/, 1 /*!exclude_seqs*/, from - 1, to);
              if (lifted_exon->strand == '-') msa_reverse_compl(sub_msa);
              msa_concatenate (foldedMSA,sub_msa);
#ifdef DEBUG
              /* This is super expensive,*/
              char *foldedMAFfile = malloc(strlen(f->name->chars) + 2 + strlen(".folded.maf") + 1);
              sprintf(foldedMAFfile, "%s.%d.sub_msa.maf", f->name->chars,j);
              msa_print_to_file(foldedMAFfile, sub_msa, MAF, 2000);
              free(foldedMAFfile);
#endif
              msa_free(sub_msa);

          }
          if (pars.missing_as_indels)
              msa_missing_to_gaps(foldedMSA, refseqIndex);

          //map = msa_build_coord_map(foldedMSA, refseqIndex);

          //foldedMSA->idx_offset = 0;
          if (cm != NULL) foldedMSA->ncats = cm->ncats;
          //else foldedMSA->ncats = -1;

          // This if doesn't look like a case that can handle my bug, so why not
          // commenting it.
          //if (foldedMSA->ss == NULL)
          //    ss_from_msas(foldedMSA, tuple_size, store_order, NULL/*cats_to_do*/, NULL/*source_msa*/, NULL/*existing_hash*/, -1/*idx_offset*/, 0/*non_overlapping*/);
          /*if(foldedMSA->ss != NULL){
              ss_to_msa(foldedMSA);
              ss_free(foldedMSA->ss);
              foldedMSA->ss = NULL;
          }*/
  
          //ss_from_msas(foldedMSA, tuple_size, store_order, cats_to_do/*cats_to_do*/, NULL/*source_msa*/, NULL/*existing_hash*/, -1/*idx_offset*/, 0/*non_overlapping*/);

          OrgfoldedMSA = msa_create_copy(foldedMSA,0); // you don't need this you can do it by cats_to_do (i.e. only 3rd and 6th position)

          //aggregate to 4d unordered Sufficient Statistics (SS)
          // You might need to interface to your maf before here!

          //int prev_exon_end = 0;
          /*for(int j=0; j<lst_size(subgff->features);j++){ 
              GFF_Feature *exon = lst_get_ptr(subgff->features, j);
              fprintf(stderr,"%d , %d\n",exon->start,exon->end);
          }*/

          //This projection is going to have no effect on me!
          //msa_map_gff_coords(foldedMSA, subgff, 1 ,refseqIndex, 0);

          //TODO: Check if you want it or not and then uncomment it;
          //if (reverse_groups_tag != NULL) { /* reverse complement by group */
          //    if (foldedMSA->ss != NULL) {
          //        ss_to_msa(foldedMSA);
          //        ss_free(foldedMSA->ss);
          //        foldedMSA->ss = NULL;
          //    }
          //    gff_group(subgff, reverse_groups_tag);
          //    msa_reverse_compl_feats(foldedMSA, subgff, NULL);
          //}

          /// for testing 
          
          /*char *groupedGFFfile = malloc(strlen(pars.gffFile) + strlen(".map.coords.gff") + 1);
          strcpy(groupedGFFfile, pars.gffFile);
          strcat(groupedGFFfile, ".map.coords.gff");
          FILE *gffSets = phast_fopen(groupedGFFfile, "w");
          gff_print_set(gffSets, subgff);
          phast_fclose(gffSets);
          fprintf(stderr, "maped coords gff stored in %s file.\n",groupedGFFfile);
          free(groupedGFFfile);*/

#ifdef DEBUG
          msa_print_to_file(f->name->chars, foldedMSA, MAF, 2000);//tmp.merged*/
#endif

          if (cm != NULL){
                  msa_label_categories(foldedMSA,subgff,cm);
          }

          gap_strip_mode = msa_get_seq_idx(foldedMSA, fourD_refseq->chars)+1;
          msa_strip_gaps(foldedMSA, gap_strip_mode);
#ifdef DEBUG
          msa_print_to_file("tmp.strip.merged", foldedMSA, MAF, 2000);//tmp.merged
#endif

          if(pars.fourD){
              //Reducing to four fold degenerate sites. This result only contain
              //those sites in which their AA production does not change by changing
              //the 3rd positions (i.e. UU*, AU*, UA*, CA*, GA*, UG*, AG* are
              //**not** included)
              reduce_to_4d(foldedMSA,cm); //you don't need this you can do it by cats_to_do (i.e. only 3rd and 6th position)
              if (foldedMSA->ss == NULL)
                  ss_from_msas(foldedMSA, tuple_size, store_order, cats_to_do/*cats_to_do*/, NULL/*source_msa*/, NULL/*existing_hash*/, -1/*idx_offset*/, 0/*non_overlapping*/);
              else{
                  if (foldedMSA->ss->tuple_size < tuple_size){
                      fprintf(stderr,"ERROR: input tuple size must be at least as large as output tuple size.\n");
                      exit(EXIT_FAILURE);
                  }
                  if (foldedMSA->ss->tuple_idx != NULL && store_order == 0) {
                      sfree(foldedMSA->ss->tuple_idx);
                      foldedMSA->ss->tuple_idx = NULL;
                  }
              }
              
#ifdef DEBUG
              msa_print_to_file("tmp.4d.strip.merged", foldedMSA, MAF, 2000);//tmp.merged
#endif
              if (foldedMSA->ss->tuple_size > tuple_size)
                  foldedMSA->ss->tuple_size = 1;

          }
          gff_free_set(subgff);

          //agreegate_previous_feature += lst_size(f->features);
          //you have your aggregated msa here!
          // msa_free_categories(foldedMSA);
          int nInformativeSites = msa_ninformative_sites(foldedMSA, -1);
          if(!pars.quiet){ 
              fprintf(stderr, "folded Seq has %ibp", OrgfoldedMSA->length);
              if (pars.fourD){
                  fprintf (stderr, " of which %ibp have valid synonumous four-degenrated site)", nInformativeSites);
              }
              fprintf (stderr, "\n");
          }
          //inofrmative in which category
          // take it into account if you are going to use cm and cat_to_do
          if (!pars.fourD || (pars.fourD && (nInformativeSites >= pf->nsites_threshold))) {

              pf->tree = specTree;
              pf->msa = foldedMSA;
              // See if you need these guys, they are supper powerfull and
              // confusing. phylo_fit.c:976
              // pf->cm;
              // pf->cats_to_do_str;
              run_phyloFit(pf);
    
              ListOfLists *currLOL = lol_find_lol(pf->results,"");

              TreeModel *mpty = interface_LOL(currLOL);
              treeString=tr_to_string(mpty->tree,1);
              kappa=0;
              
              if(mpty!=NULL){
              //if(extractResult(currLOL,&treeString, &kappa)){
                  int withHeader=0;
                  for (int from=0; from < OrgfoldedMSA->length; from+=pars.windowSize){
                      int numberOfspec = 0;
                      if( pars.windowSize <= OrgfoldedMSA->length){
                           fprintf(stderr, " |---> window region (%li,%li)\n",from,from+pars.windowSize);
                           //to interface becarfull to not include the species which
                           //are not in the tree!
                           numberOfspec = interface_SS(OrgfoldedMSA,inputAln,from,from+pars.windowSize,specTree);
                           withHeader=1;
                      } else {
                           numberOfspec = interface_SS(OrgfoldedMSA,inputAln, -1, -1,specTree);
                      }
                      fprintf(stderr, "Scoring %i species\n", numberOfspec);
                      if (numberOfspec > 0){
                          snugFit((const struct aln**)inputAln, (const struct aln**)inputAlnRev,treeString,kappa,f,withHeader,mpty);
                      } else {
                          fprintf(stderr, "NMI, none of species in a given tree are in the alignment!!\n", numberOfspec);
                          exit(EXIT_FAILURE);
                      }
                  }
                  fprintf(stderr, "\n");

              } else {

                  fprintf(stderr, "Error: Couldn't find any fitted model!!!\n");
                  exit(EXIT_FAILURE);
              }

              if(mpty != NULL)
              {
                  tm_free(mpty);
              }
              
              
              //msa_free(OrgfoldedMSA);
              //msa_free(foldedMSA);
              //phyloFit_struct_delete(pf);// msa_free(foldedMSA) is part of phylofitStruct_delete
              //sfree(pf);
              //sfree(subgroup);

          } else {
              fprintf(stderr, "WARNING: skipping %s insufficient informative sites on 4d...!\n\n",f->name->chars);
                  /*if (foldedMSA != NULL){
                      msa_free(foldedMSA);
                  }
                  if (OrgfoldedMSA != NULL){
                      msa_free(OrgfoldedMSA);
                  }
                  pf->msa = NULL;*/
              //msa_free(OrgfoldedMSA);
              //msa_free(foldedMSA);
          }
          if (foldedMSA != NULL){
              msa_free(foldedMSA);
          }
          if (OrgfoldedMSA != NULL){
              msa_free(OrgfoldedMSA);
          }

          /*if (treeString != NULL){
              free(treeString);
              treeString = NULL;
          }*/

          pf->msa = NULL;
        
      }
  
  /* Chek if you need to free msas** as well*/
  //for (int counter=0; counter <lst_size(msas); counter++ ){
  //   msa_free(msas[counter]);
  //}
  //msa_free(msas);
  //msa_free(OrgfoldedMSA);
  //msa_free(sub_msa);
  
  } else {
      fprintf (stderr, " I am too tired to reorganisi this code to support the case with no gff! Feel free to add this \"feature\")\n");
  }
  //msa_free(OrgfoldedMSA);
  //
  
  msa_free(mainMSA);

  /*if (theseSeqs != NULL){
      lst_free(theseSeqs);
  }*/

  if(includeSeqs != NULL) {
      lst_free_strings(includeSeqs);
      lst_free(includeSeqs);
  }
  //tr_free(SpeciesTREE);
  if(cm !=NULL){
      cm_free(cm);
  }

  if(pf != NULL){
      //phyloFit_struct_delete(pf);
      sfree(pf);
  }

  if(fourD_refseq != NULL){
      str_free(fourD_refseq);
  }
  if(cats_to_do != NULL){
      lst_free(cats_to_do); // this is just because we didn't assign them into pf!
  }

  if(gff!=NULL){
      gff_free_set(gff);
  }

  if(liftedGFF != NULL){
      gff_free_set(liftedGFF);
  }

  if(map!=NULL){
      msa_map_free(map);
  }


  if (pars.outputFormat==0){

    runtime = (float)(clock() - startTime) / CLOCKS_PER_SEC;

    fprintf(pars.outputFile,
            "\n%i alignment(s) scored in %.2f seconds. Parameters used:\nN=%i, Delta=%.2f, Omega=%.2f, omega=%.2f, stop penalty=%.2f\n\n", 1 /*alnCounter*/,runtime,pars.sampleN, pars.Delta,pars.Omega,pars.omega,pars.stopPenalty_k);
  }

  phast_fclose(pars.inputFile);
  phast_fclose(pars.outputFile);

  exit(EXIT_SUCCESS);

}

