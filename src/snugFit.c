
#include "extreme_fit.h"
#include "score.h"
#include "postscript.h"
#include "code.h"
#include "distance.h"
#include "joinery_misc.h"

#include "gff.h"
#include "tree_model.h"

extern float**** Sk;
extern float**** Sk_native;   
extern float**** Sk_native_rev;  
extern bgModel *models;
extern bgModel *modelsRev;
extern long int hitCounter;

//TODO: I don;t need treeString, kappa and probbably inputAlnRev!!!
int snugFit( const struct aln *inputAln[], const struct aln *inputAlnRev[], char *treeString, double kappa, GFF_FeatureGroup *featureGroup, int withHeader, TreeModel *mod) {

  int i,j,k,x,L,N,hssCount, alnCounter;
  float /*kappa,*/ maxScore;
  //TTree* tree;
  segmentStats *results;
  float parMu, parLambda;
  //clock_t startTime;	
  //float runtime;
  
  alnCounter=0;
  hitCounter=0;
  //startTime=clock();

  //struct aln *inputAln[MAX_NUM_NAMES];
  //struct aln *inputAlnRev[MAX_NUM_NAMES];


  alnCounter++;




    /* Currently repeat masked regions are ignored*/
    /* Fix this */
    L=0;
    //TODO: Limit your species, Do I want it?
    //if (!strcmp(pars.limit,"")==0){
    //  pruneAln(pars.limit,(struct aln**)inputAln);
    //}
    
    //printAlnMAF(stdout,(const struct aln**)inputAln,0); 
    
    //Moved inside the uper for to check for length of all inputAln to be
    //greater than 2
    //L=getSeqLength(inputAln[0]->seq);
    
    for (N=0; inputAln[N]!=NULL; N++);
    
    /* Currently minimum number of sequences is 3 because BIONJ seg-faults with 2 */
    /* Fix this that it works with two sequences*/
    //if (N<=2){
    //  fprintf(stderr,"Skipping alignment (%d,%d). There must be at least three sequences in the alignment.\n",inputAln[0]->start,inputAln[0]->start+inputAln[0]->length);
    //  continue;
    //}

    //if (((L * 1.0)/N)<3){ //check for length of all inputAln to be greater than 2
    //  fprintf(stderr,"Skipping alignment (%d,%d). Too short.\n",inputAln[0]->start,inputAln[0]->start+inputAln[0]->length);
    //  continue;
    //}

    // Back to original counting because of free stage
    L=getSeqLength(inputAln[0]->seq); 
    
   // if (treeML_phast((const struct aln**)inputAln,&treeString,&kappa)==0){
   //   fprintf(stderr,"\nSkipping alignment (%d,%d). Failed to build ML tree.\n",inputAln[0]->start,inputAln[0]->start+inputAln[0]->length);
    //  continue;
   // }


    //tree=string2tree(treeString); // TODO: You have mod, which contain tree. mod->tree
    
    copyAln((struct aln**)inputAln,(struct aln**)inputAlnRev);
    revAln((struct aln**)inputAlnRev);

    models=getModels(mod,(struct aln**)inputAln,kappa);
    modelsRev=getModels(mod,(struct aln**)inputAlnRev,kappa);

    Sk=NULL;
    Sk_native=NULL;
    Sk_native_rev=NULL;

    results=scoreAln((const struct aln**)inputAln, /*tree, kappa,*/ 1);
    
    hssCount = 0;
    while (results[hssCount++].score > 0.0);

    qsort((segmentStats*) results, hssCount,sizeof(segmentStats),compareScores);

    maxScore=results[0].score;

    /*if(maxScore >=0.0){
        fprintf(stderr,"(%d,%d) -> %f. \n",results[0].startGenomic,results[0].endGenomic,maxScore);
        fprintf(stdout,"-------------------\n");
    }*/


    if (modeBaseGetExtremeValuePars(mod, (const struct aln**)inputAln, maxScore, &parMu, &parLambda) == 1){
    //if (getExtremeValuePars(tree, (const struct aln**)inputAln, pars.sampleN, maxScore, &parMu, &parLambda) == 1){
      for (i=0;i<hssCount;i++){
        results[i].pvalue=1-exp((-1)*exp((-1)*parLambda*(results[i].score-parMu)));
      }
    } else {
      for (i=0;i<hssCount;i++){
        results[i].pvalue=99.0;
      }
    }

    //printResults(pars.outputFile,pars.outputFormat,(const struct aln**)inputAln, results,featureGroup,withHeader,treeString);
    //TODO: Print results doesn't fit here it is better to be in Main.
    printResults((const struct aln**)inputAln, results,featureGroup,withHeader,treeString);

    if(Sk != NULL || Sk_native != NULL || Sk_native_rev!= NULL){
        for (k=0;k<N;k++){
          for (x=0;x<3;x++){
            for (i=0;i<L+1;i++){
              free(Sk[k][x][i]);
              free(Sk_native[k][x][i]);
              free(Sk_native_rev[k][x][i]);
            }
            free(Sk[k][x]);
            free(Sk_native[k][x]);
            free(Sk_native_rev[k][x]);
          }
          free(Sk[k]);
          free(Sk_native[k]);
          free(Sk_native_rev[k]);
        } 
    }
    free(Sk);
    free(Sk_native);
    free(Sk_native_rev);
    Sk=NULL;
    Sk_native=NULL;
    Sk_native_rev=NULL;


    //TODO:freeSeqgenTree(tree);
    freeResults(results);
    freeModels(models,N);
    freeModels(modelsRev,N);
    freeAln((struct aln**)inputAln);
    freeAln((struct aln**)inputAlnRev);
    //free(treeString);

    return 0;
}
