/*  Copyright 2017, Amin Saffari

    This file is part of Joinery.

    Joinery is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Joinery is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RNAcode.  If not, see <http://www.gnu.org/licenses/>. */



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <math.h>

#include "distance.h"
#include "trees.h"


double tr_distance_to_me(TreeNode *node, const char *name) {
  if (node->parent == NULL) return 0.0;
  else if (node->name[0] != '\0' && !strcmp(node->name, name)){
     return 0.0;
  }
  return node->dparent + tr_distance_to_me(node->parent,name);
}

/* getDistanceMatrix, fill the matrix distance between every two pairs of the 
 * species. For that we first find the LCA of the two species and then 
 * calculate the distance of them fom each others.
 */

float **getDistanceMatrix(TreeNode *tree){
  TreeNode *LCA=NULL, *nodeA, *nodeB;
  float **matrix;
  List *pairSpecies=lst_new_ptr(2), *species = tr_leaf_names(tree);
  int i,j,nSpecies = lst_size(species);
  if (nSpecies < 2){
    fprintf(stderr, "Error: Can't calculate the distance for a tree of length 1!!\n");
    exit(EXIT_FAILURE);
  }

  matrix=(float**)malloc(sizeof(float*)*nSpecies);
  for (i=0;i<nSpecies;++i){
    matrix[i]=(float*)malloc(sizeof(float)*nSpecies);
  }

  for(i=0;i<nSpecies;++i){
    for(j=0;j<=i;++j){
      if (i==j){
        matrix[i][j]=0.0;
        continue;
      }

      LCA=NULL;
      nodeA=NULL;
      nodeB=NULL;
      pairSpecies=NULL;

      nodeA = lst_get_ptr(tree->nodes, i);
      nodeB = lst_get_ptr(tree->nodes, j);
      
      lst_push_ptr(pairSpecies, nodeA);
      lst_push_ptr(pairSpecies, nodeB);
      LCA = tr_lca(tree, pairSpecies);
      matrix[i][j]=tr_distance_to_me(nodeA,LCA->name)+tr_distance_to_me(nodeB,LCA->name);
      matrix[j][i]=matrix[i][j];
    }

  }

  return matrix;

}

