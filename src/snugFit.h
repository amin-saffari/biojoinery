#ifndef SNUG_FIT
#define SNUG_FIT

#include "gff.h"
#include "tree_model.h"

int snugFit( const struct aln *inputAln[], const struct aln *inputAlnRev[], char *treeString, double kappa, GFF_FeatureGroup *featureGroup, int withHeader, TreeModel *mod);

#endif
