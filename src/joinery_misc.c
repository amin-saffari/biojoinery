/*  Copyright 2009, Stefan Washietl

    This file is part of RNAcode.

    RNAcode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    RNAcode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with RNAcode.  If not, see <http://www.gnu.org/licenses/>. */


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/stat.h>
#include <errno.h>
#include "RNAcode.h"
#include "joinery_misc.h"
#include "gff.h"
#include "msa.h"
#include "trees.h"

extern parameters pars;

extern long int hitCounter;

float**** allocateSk(int N, int L){

  float**** S;
  int i, k, x;
  
  // We have one entry for each pair
  S=(float****)malloc(sizeof(float***)*(N+1));

  for (k=0;k<N;k++){
    // We have three states
    S[k]=(float***)malloc(sizeof(float**)*(3));
    
    for (x=0;x<3;x++){
     // indices are 1 based and we mark end with NULL, so we need L+2
      S[k][x]=(float**)malloc(sizeof(float*)*(L+1));
      
      for (i=0;i<L+1;i++){
        S[k][x][i]=(float*)malloc(sizeof(float)*(L+1));
      }
    }
  }
  return S;
}

void copySk(float**** from, float**** to, int N, int L){

  int i, j, k, x;
  
  for (k=0;k<N;k++){
    for (x=0;x<3;x++){
      for (i=0;i<L+1;i++){
        for (j=0;j<L+1;j++){
          to[k][x][i][j] = from[k][x][i][j];
        }
      }
    }
  }

}




/*********************************************************************
  compareScores

  compare function for qsort that compares two scores given as pointer
  to a segmentStats structure.

*********************************************************************/ 

int compareScores(const void * a, const void * b){

  segmentStats* statsA;
  segmentStats* statsB;

  statsA=(segmentStats*)a;
  statsB=(segmentStats*)b;

  if  ( statsA->score < statsB->score) {
    return 1;
  } else {
    return -1;
  }

  return 0;
}

/*********************************************************************
  compareLocation

  compare function for qsort that compares start positions given as
  pointer to a segmentStats structure.

*********************************************************************/ 

int compareLocation(const void * a, const void * b){

  segmentStats* statsA;
  segmentStats* statsB;

  statsA=(segmentStats*)a;
  statsB=(segmentStats*)b;

  if  ( statsA->startSite > statsB->startSite) {
    return +1;
  } else {
    return -1;
  }

  return 0;
}


void reintroduceGaps(const struct aln* origAln[], struct aln* sampledAln[]){

  int i,j,k;

  char* tmpSeq;
  char* tmpName;
  char* origSeq;
  char* sampledSeq;


  for (i=0;origAln[i]!=NULL;i++){

    origSeq=origAln[i]->seq;
    sampledSeq=sampledAln[i]->seq;
    
    for (k=0;k<strlen(origSeq);k++){
      if (origSeq[k]== GAP_CHAR){
        sampledSeq[k]= GAP_CHAR ;
      }
    }
  }
}

void sortAln(const struct aln* origAln[], struct aln* sampledAln[]){

  int i,j,k;

  char* tmpSeq;
  char* tmpName;
  char* origSeq;
  char* sampledSeq;

  for (i=0;origAln[i]!=NULL;i++){
    for (j=0;sampledAln[j]!=NULL;j++){
      if (strcmp(origAln[i]->name,sampledAln[j]->name)==0){
        tmpSeq=sampledAln[j]->seq;
        tmpName=sampledAln[j]->name;
        sampledAln[j]->seq=sampledAln[i]->seq;
        sampledAln[j]->name=sampledAln[i]->name;
        sampledAln[i]->seq=tmpSeq;
        sampledAln[i]->name=tmpName;
      }
    }
  }
}

/*

returns block of potential codon that ends in i; the block contains leading gaps

seq_0    ... whole reference sequence
seq_k    ... whole sequence k
i        ... sequence position in reference sequence (1-based)
block_0  ... Block in reference sequence that is to be returned
block_k  ... Block in sequence k that is to be returned
z        ... difference of gaps between reference and k in the block

*/

void getBlock(int i, const char* seq_0, const char* seq_k, const int* map_0, const int* map_k, char* block_0, char* block_k, int* z ){

  int start,end;
  int gap_0, gap_k;
  int diff;
  char c;

  if (i<3){
    fprintf(stderr, "Fatal error in getBlock. i needs to be >=3");
    exit(EXIT_FAILURE);
  }

  if (i>3){
    //start=pos2col(seq_0,i-3)+1;
    start=map_0[i-3]+1;
  }

  if (i==3){
    start=1;
  }
  
  //end=pos2col(seq_0,i);
  end=map_0[i];

  strncpy(block_0, seq_0+start-1,end-start+1);
  strncpy(block_k, seq_k+start-1,end-start+1);
  
  block_0[end-start+1]='\0';
  block_k[end-start+1]='\0';

  gap_0 = gap_k = 0;

  i=0;
  while (block_0[i] != '\0'){
    if (block_0[i] == GAP_CHAR) gap_0++;
    i++;
  }

  i=0;
  while (block_k[i] != '\0'){
    if (block_k[i] == GAP_CHAR) gap_k++;
    i++;
  }

  diff=gap_k-gap_0;
  if (diff<0) diff*=-1;


  if ( diff % 3 == 0 ){
    *z=0;
  }

  if ( diff % 3 == 1 ){
    *z=+1;
  }

  if ( diff % 3 == 2 ){
    *z=-1;
  }

}

// both pos and col are 1 based!

int pos2col(const char* seq, int pos){

  int i=0;
  int currPos=0;
    
  while (seq[i] != '\0'){
  
    if (seq[i]== GAP_CHAR){
      i++;
      continue;
    } else {
      currPos++;
    }

    if (currPos==pos){
      return i+1;
    }
    i++;
  }
}


int getSeqLength(char* seq){

  int i=0;
  int counter=0;
  
  while (seq[i] != '\0'){
    if (seq[i]== GAP_CHAR){
      i++;
      continue;
    } else {
      counter++;
      i++;
    }
  }

  return counter;

}


void freeResults(segmentStats results[]){
  
  int i=0;

  while (results[i].score>0){
    free(results[i].name);
    i++;
  }
  free(results);
}

int hDist(int a1, int a2, int a3, int b1, int b2, int b3){

  int dist=0;

  if (a1 != b1) dist++;
  if (a2 != b2) dist++;
  if (a3 != b3) dist++;
 
  return dist;

}


float avg(float* data, int N){

  int i;
  float sum;

  sum=0.0;

  for (i=0;i<N;i++){
    sum+=data[i];
  }
  
  return sum/(float)N;

}


float stddev(float* data, int N){

  int i;
  float sum;
  float mean;

  sum=0.0;

  for (i=0;i<N;i++){
    sum+=data[i];
  }

  mean=sum/(float)N;

  sum=0.0;

  for (i=0;i<N;i++){
    sum+=(mean-data[i])*(mean-data[i]);
  }
  
  return sqrt(sum/(float)(N-1));
}

void copyAln(struct aln *src[],struct aln *dest[]){

  int i,j,L;
  char* seq;
  
  L=strlen(src[0]->seq);

  for (i=0;src[i]!=NULL;i++){
    
    dest[i]=createAlnEntry(strdup(src[i]->name),
                           strdup(src[i]->seq),
                           src[i]->start, 
                           src[i]->length,
                           src[i]->fullLength,
                           src[i]->strand);
  }

  dest[i]=NULL;
}



void printAlnClustal(FILE *out, const struct aln* AS[]){

  int i;
  
  fprintf(out,"CLUSTAL W\n\n");

  for (i=0;AS[i]!=NULL;i++){
    fprintf(out, "%s %s\n",AS[i]->name,AS[i]->seq);
  }

  fprintf(out, "\n");

}


void printResults(const struct aln* inputAln[], segmentStats results[],GFF_FeatureGroup *featureGroup,int withHeader, char* tree){

  int i,k, hssCount, currHSS, nextHSS;
  char c;
  char name[1024]="";
  char fileName[2048]="";
  char prefix[1024]="";
  char suffix[1024]="";
  TreeNode *fittedTree = NULL;

  FILE *outfile = pars.outputFile;
  int outputFormat = pars.outputFormat;

  hssCount = 0;
  while (results[hssCount].score > 0.0){
    results[hssCount].hide=0;
    //printf("Inner %.2f (%i)\n", results[hssCount].score, results[hssCount].hide);
    hssCount++;
  }
    
  if (pars.bestRegion){
  
    /* First sort by start position */
    qsort((segmentStats*) results, hssCount,sizeof(segmentStats),compareLocation);
    currHSS=0;
    nextHSS=1;
    while (nextHSS<=hssCount) {
    
      /* the two HSS overlap */
      if (!(results[currHSS].endSite <= results[nextHSS].startSite)){
      
        /* Hide the HSS with lower score */
        if (results[currHSS].score > results[nextHSS].score){
          results[nextHSS].hide=1;
          nextHSS++;
        } else {
          results[currHSS].hide=1;
          currHSS=nextHSS;
          nextHSS++;
        }
      } else {
        currHSS=nextHSS;
        nextHSS++;
      }
    }
  }

  qsort((segmentStats*) results, hssCount,sizeof(segmentStats),compareScores);

  if (results[0].score<0.0 || results[0].pvalue > pars.cutoff){
    if (outputFormat==0){
      fprintf(outfile,"\nNo significant coding regions found.\n");
    }
    return;
  } 

  if (outputFormat==0){
    fprintf(outfile, "\n%6s%5s%7s%6s%6s%12s%12s%12s%9s%9s\n",
            " HSS # ", "Frame","Length","From","To","Name","Start","End", "Score","P");
    fprintf(outfile, "======================================================================================\n");
  }

  
  i=0;

  while (results[i].score>0.0 && results[i].pvalue < pars.cutoff){
    
    if (results[i].hide) {
      i++;
      continue;
    }


    if (pars.postscript){
      if (results[i].pvalue < pars.postscript_cutoff){
        fittedTree = tr_new_from_string(tree);
        struct stat stat_p;	
        
        if (stat (pars.postscriptDir, &stat_p) != 0){
          if (mkdir(pars.postscriptDir, S_IRWXU|S_IROTH|S_IRGRP ) !=0){
            fprintf(stderr, "WARNING: Could not create directory: %s", pars.postscriptDir);
          }
        }
        
        sprintf(fileName,"%s/hss-%li-%s.eps", pars.postscriptDir,hitCounter, featureGroup!=NULL ? featureGroup->name->chars:""); 
        colorAln(fileName,(const struct aln**)inputAln, results[i]);
        if (fittedTree != NULL){
           //sprintf(fileName,"%s/tree-%li-%s.eps", pars.postscriptDir,hitCounter, featureGroup!=NULL ? featureGroup->name->chars:"");
           //FILE *treeoutfile = phast_fopen(fileName,"w");
           //tr_print_ps(stdout, tree, show_branch_lens, square_branches, draw_to_scale,horizontal);
           //tr_print_ps(treeoutfile,fittedTree,1,1,1,1);
           tr_free(fittedTree);
           //phast_fclose(treeoutfile);
        }
      }
    }


    if (outputFormat==0){

      fprintf(outfile, "%6li %4c%i%7i%6i%6i%12s%12i%12i%9.2f",
              hitCounter,
              results[i].strand, results[i].frame+1,
              results[i].endSite-results[i].startSite+1,
              results[i].startSite+1,results[i].endSite+1,
              results[i].name,
              results[i].startGenomic,results[i].endGenomic,
              results[i].score);

        if (results[i].pvalue < 0.001){
          /*Seems to be the minimum number I can get, don't know why
            we don't get down to 1e-37 which should be the limit for
            floats.
          */
          if (results[i].pvalue < 10e-16){
            fprintf(outfile, "   <1e-16\n");
          } else {
            fprintf(outfile, "% 9.1e\n",results[i].pvalue);
          }

        } else {
          fprintf(outfile, "% 9.3f\n",results[i].pvalue);
        }
    }

    if (outputFormat==1){
      if(featureGroup == NULL){
          /* if name is of the form hg18.chromX than only display chromX */
          k=0;
          while (1){
            if (results[i].name[k]=='\0' || results[i].name[k]=='.'){
              break;
            }
            k++;
          }
    
          if (k==strlen((char*)results[i].name)){
            strcpy(name,results[i].name);
          } else {
            strcpy(name,results[i].name+k+1);
          }
    
          fprintf(outfile,"%s\t%s\t%s\t%i\t%i\t%.2f|%.2e\t%c\t%c\t%s%li%s\n",
                  name, "Joinery","CDS",
                  results[i].startGenomic+1,results[i].endGenomic+1,
                  results[i].score,
                  results[i].pvalue,
                  results[i].strand, '.',"gene_id \"Gene", hitCounter ,"\"; transcript_id \"transcript 0\";");
      } else {
          /* if name is of the form hg18.chromX than only display chromX */
          k=0;
          while (1){
            if (results[i].name[k]=='\0' || results[i].name[k]=='.'){
              break;
            }
            k++;
          }
    
          if (k==strlen((char*)results[i].name)){
            strcpy(name,results[i].name);
          } else {
            strcpy(name,results[i].name+k+1);
          }

          if(withHeader == 0){ // I am not in window by window mode
             
             fprintf(outfile,"%s\t%s\t%s\t%i\t%i\t%.2f|%.2e\t%c\t%c\t%s%li%s%li%s%s%s\n",
                     name, "Joinery","transcript",
                     featureGroup->start,featureGroup->end,
                     results[i].score,
                     results[i].pvalue,
                     results[i].strand, '.',
                     "gene_id \"", hitCounter ,"\"; transcript_id \"",hitCounter,".",featureGroup->name->chars,"\";");
          } else { // I am in window by window
             if ( withHeader == 1 && i == 0) { // and that's why only print the header for the first window and first result
                 fprintf(outfile,"%s\t%s\t%s\t%i\t%i\t%.2f|%.2e\t%c\t%c\t%s%li%s%li%s%s%s\n",
                       name, "Joinery","transcript",
                       featureGroup->start,featureGroup->end,
                       results[i].score,
                       results[i].pvalue,
                       results[i].strand, '.',
                       "gene_id \"", hitCounter ,"\"; transcript_id \"",hitCounter,".",featureGroup->name->chars,"\";");
             }
          }

          int leftOver = 0;
          long int curStart = results[i].startGenomic+1;
          long int curEnd = results[i].endGenomic+1;
          long int start= -1, end=-1;
          for (int featureIndex=0; featureIndex < lst_size(featureGroup->features); featureIndex++){ /*This for should only affect start and end part and not header*/
              GFF_Feature *feature = lst_get_ptr(featureGroup->features, featureIndex); 

              if ( (feature->start <= curStart) && (curStart < feature->end)){ // - feature->start)){
                  start = curStart;
              } else{
                  if(curStart < feature->start){
                      curEnd += (feature->start - curStart +1);
                      start = feature->start;
                  } else {
                      start = -1;
                  }
              }
              if (curEnd < feature->end){
                  end = curEnd;
              } else{
                  end = feature->end ;
              }

              if (start >= 0){
               curStart = end;
               //curEnd -= end;
               fprintf(outfile,"%s\t%s\t%s\t%li\t%li\t%.2f|%.2e\t%c\t%c\t%s%li%s%li%s%s%s\n",
                  name, "Joinery","CDS",
                  start,end, /* Project this coordinate based on your gff*/
                  results[i].score,
                  results[i].pvalue,
                  results[i].strand, '.',
                  "gene_id \"", hitCounter ,"\"; transcript_id \"",hitCounter,".",featureGroup->name->chars,"\";");
              }
              //if (start == end){
              //   break ; 
              //}
              
          }
      }
    }
  
    if (outputFormat==2){

      fprintf(outfile, "%li\t%c\t%i\t%i\t%i\t%i\t%s\t%i\t%i\t%7.3f\t",
              hitCounter,
              results[i].strand, results[i].frame+1,
              results[i].endSite-results[i].startSite+1,
              results[i].startSite+1,results[i].endSite+1,
              results[i].name,
              results[i].startGenomic,results[i].endGenomic,
              results[i].score);
      
      if (results[i].pvalue < 0.001){
        fprintf(outfile, "% 9.3e\n",results[i].pvalue);
      } else {
        fprintf(outfile, "% 9.3f\n",results[i].pvalue);
      }
    }
    i++;

    if (pars.bestOnly) break;

    if (featureGroup == NULL){
        hitCounter++;
    }

  }
  fflush(outfile);
}


int extendRegion(const struct aln* alignment[], int pos, int direction){

  int *map_0, *map_k;
  int N, k, x, z, L, l, colsN;
  char *block_0, *block_k, *seq;
  int pepA;
  int ii, jj;
  char codonA[4];
  
  seq = alignment[0]->seq;

  L=getSeqLength(seq);
  colsN=strlen(seq);

  block_0 = (char*) malloc(sizeof(char)*(colsN+1));
  block_k = (char*) malloc(sizeof(char)*(colsN+1));
  
  map_0=(int*)malloc(sizeof(int)*(colsN+1));
  map_k=(int*)malloc(sizeof(int)*(colsN+1));

  //printf("Pos %i L %i\n", pos, L);

  for (l=1;l<=L;l++){
    map_0[l]=pos2col(seq,l);
    map_k[l]=pos2col(alignment[1]->seq,l);
  }
  
  if (direction == 0 ){
    x=pos+2;
  } else {
    x=pos;
  }

  while (1){
    
    getBlock(x, seq, alignment[1]->seq, map_0, map_k, block_0, block_k, &z );

    ii=jj=0;
      
    while (block_0[ii] != '\0') {
      if (block_0[ii] != GAP_CHAR) {
        codonA[jj]=block_0[ii];
        jj++;
      }
      ii++;
    }

    pepA=transcode[ntMap[codonA[0]]][ntMap[codonA[1]]][ntMap[codonA[2]]];

    if (pepA == -1) break;

    if (direction == 0){
      if (x-3<3) break;
      x-=3;

    } else {
      if (x+3 > L) break;
      x+=3;
    }
  }

  free(block_0);
  free(block_k);
  free(map_0);
  free(map_k);




  if (direction == 0){
    return x-2;
  } else {
    return x;
  }


}


msa_coord_map *lift_coordinate(MSA *mainMSA, GFF_Set *liftedGFF, int refseqIndex){
    msa_coord_map *map = msa_build_coord_map(mainMSA, refseqIndex);
#ifdef DEBUG
   TMPoutfile = phast_fopen("map.dump", "w");
   msa_coord_map_print(TMPoutfile,map);
   phast_fclose(TMPoutfile);
#endif
   if(!pars.quiet){
       fprintf(stdout, "add offset and project coordinats into the frame of the alignment\n");
   }
   if(liftedGFF != NULL){
       /* need to remap to coord frame of alignment and add offset */
       if (mainMSA->idx_offset != 0) {
           for (int i=0; i<lst_size(liftedGFF->features); i++) {
               GFF_Feature *f = lst_get_ptr(liftedGFF->features, i);
               f->start -= mainMSA->idx_offset;
               f->end -= mainMSA->idx_offset;
           }
       }
   }
   msa_map_gff_coords(mainMSA, liftedGFF, refseqIndex, 0, 0); //
   if (lst_size(liftedGFF->features) == 0) {
       //non-overlapping features removed!!
       fprintf(stdout, "All features were non-overlapping features!\n");
       exit(EXIT_FAILURE);

   }
   if(pars.transcriptgroup_tag!=NULL){
       gff_group(liftedGFF,pars.transcriptgroup_tag);
       if(pars.exongroup_tag!=NULL){
           gff_group(liftedGFF,pars.exongroup_tag);
           gff_group(liftedGFF,pars.transcriptgroup_tag);
       }
   }
   gff_sort(liftedGFF);
#ifdef DEBUG
   char *groupedGFFfile = malloc(strlen(pars.gffFile) + strlen(".lifted.gff") + 1);
   strcpy(groupedGFFfile, pars.gffFile);
   strcat(groupedGFFfile, ".lifted.gff");
   FILE *gffSets = phast_fopen(groupedGFFfile, "w");
   gff_print_set(gffSets, liftedGFF);
   phast_fclose(gffSets);
   fprintf(stderr, "sorted gff stored in %s file.\n",groupedGFFfile);
   free(groupedGFFfile);
#endif
   return map;
}

/* I don't wanna arite my result into a file and at the same time ListOfList
 * datastructure is not usefull if I want to use phyloBoot to simulate
 * sequences. So the "idea" is to have something like tm_new_from_file
 * (tree_model.c:367) but read from ListOfList.
 */

TreeModel *interface_LOL(ListOfLists *curLOL){
    char tag[STR_MED_LEN], alphabet[MAX_ALPH_SIZE]; 
    char *tmpstr = NULL;
    //String *tmpstr = str_new(STR_LONG_LEN);
    Vector *backgd = NULL, *rate_weights = NULL;
    Matrix *rmat = NULL;
    MarkovMatrix *M = NULL;
    TreeNode *tree = NULL;
    int size = 0, order = 0, nratecats = -1, empty = TRUE, have_likelihood=1;
    double alpha = 0, selection, likelihood;
    TreeModel *retval;
    subst_mod_type subst_mod = UNDEF_MOD;
    int i, j, have_selection=0;
    List *rate_consts = NULL;
    alphabet[0] = '\0';

    if(curLOL != NULL){
        List *tmp_tag = NULL;

        /*ALPHABET*/
        tmp_tag = lol_find_list(curLOL,"alphabet",CHAR_LIST);
        if(tmp_tag == NULL){
            fprintf(stderr, "Error: Couldn't find alphabet!!!\n");
            exit(EXIT_FAILURE);
        }
        tmpstr = (char *)lst_get_ptr(tmp_tag,0);
        j=0;
        for (i = 0; tmpstr[i] != '\0' ; i++)
            if (!isspace(tmpstr[i])) alphabet[j++] = tmpstr[i];
        alphabet[j] = '\0';
        tmp_tag = NULL;
        tmpstr=NULL;

        /*SUBST_MOD*/
        tmp_tag = lol_find_list(curLOL,"subst.mod",CHAR_LIST);
        if(tmp_tag == NULL){
            fprintf(stderr, "Error: Couldn't find subst.mod!!!\n");
            exit(EXIT_FAILURE);
        }
        tmpstr = (char*)lst_get_ptr(tmp_tag,0);
        subst_mod = tm_get_subst_mod_type(tmpstr);
        tmp_tag = NULL;
        tmpstr=NULL;

        /*NRATECAT*/
        tmp_tag = lol_find_list(curLOL,"nratecats",INT_LIST);
        if(tmp_tag != NULL){
            nratecats = lst_get_int(tmp_tag,0);
            tmp_tag = NULL;
            if(nratecats>1){
                /*ALPHA*/
                tmp_tag = lol_find_list(curLOL,"alpha",DBL_LIST);
                if(tmp_tag != NULL){
                    alpha = lst_get_dbl(tmp_tag,0);
                    if(alpha <= 0.0){
                        fprintf(stderr, "Error: Bad Alpha!!!\n");
                        exit(EXIT_FAILURE);
                    }
                }
                tmp_tag = NULL;

                /*RATE_CONSTS*/
                tmp_tag = lol_find_list(curLOL,"rate.consts",DBL_LIST);
                if(tmp_tag != NULL){
                    //tmpVec = vec_new_from_list(tmp_tag);
                    rate_consts = tmp_tag; //lst_get_dbl(tmp_tag,nratecats);
                }
                tmp_tag = NULL;
            }
        }


        /*TRAINING_LNL*/
        //If you fit your data by EM you wouldn'y get this field
        tmp_tag = lol_find_list(curLOL,"likelihood",DBL_LIST);
        if(tmp_tag == NULL){
            have_likelihood = 0;
        } else {
            likelihood = lst_get_dbl(tmp_tag,0);
        }
        tmp_tag = NULL;

        /*TREE*/
        tmp_tag = lol_find_list(curLOL,"tree",CHAR_LIST);
        if(tmp_tag == NULL){
            fprintf(stderr, "Error: Couldn't find alphabet!!!\n");
            exit(EXIT_FAILURE);
        }
        tmpstr = (char *)lst_get_ptr(tmp_tag,0);
        for (i = 0; tmpstr[i] != ';' ; i++);
        tmpstr[i] = '\0';
        tree = tr_new_from_string(tmpstr);
        tmp_tag = NULL;
        tmpstr=NULL;


        /*BACKGROUND*/
        tmp_tag = lol_find_list(curLOL,"backgd",DBL_LIST);
        if(tmp_tag == NULL){
            fprintf(stderr, "Error: Couldn't find background_freq!!!\n");
            exit(EXIT_FAILURE);
        }
        backgd = vec_new_from_list(tmp_tag);
        /*for (int colIndex = 0; colIndex < lst_size(tmp_tag); colIndex++){
            backgd[colIndex] = lst_get_dbl(tmp_tag,colIndex);
        }*/
        tmp_tag = NULL;

        /*RATE_MAT*/

        List *rawNames=NULL;
        int rawIndex=0, colIndex = 0;
        ListOfLists *rateMatrix = NULL;

        rateMatrix = lol_find_lol(curLOL,"rate.matrix");
        tmp_tag = lol_find_list(rateMatrix,"row.names",CHAR_LIST);
        double **array = NULL;
        array = (double**)smalloc((lst_size(tmp_tag)) * sizeof(double));
        for (rawIndex=0; rawIndex< lst_size(tmp_tag); rawIndex++){
            List *rateRaw = NULL;
            char *rawName = (char*)lst_get_ptr(tmp_tag, rawIndex);
            rateRaw  = lol_find_list(rateMatrix,rawName,DBL_LIST);
            if(rateRaw == NULL){
                fprintf(stderr, "Error: Couldn't find the substitution raw at index %d !!!\n",rawIndex);
                exit(EXIT_FAILURE);
            }
            array[rawIndex] = (double*)smalloc(lst_size(rateRaw) * sizeof(double));
            for (colIndex = 0; colIndex < lst_size(rateRaw); colIndex++){
                array[rawIndex][colIndex] = lst_get_dbl(rateRaw, colIndex);
                //double rate = lst_get_dbl(rateRaw, colIndex);
                //fprintf(stdout,"%f\t",rate);
            }
            //fprintf(stdout,"\n");
        }
        rmat = mat_new_from_array(array,rawIndex,colIndex);
        if(rmat!=NULL){
            M = mm_new_from_matrix(rmat, alphabet, CONTINUOUS);
        }

        /*Make the model*/

        retval = tm_new(tree, M, backgd, subst_mod, alphabet, nratecats, alpha,rate_consts, -1);

        if(have_likelihood){
            retval->lnL = likelihood;
        }


/*  while (fscanf(f, "%s", tag) != EOF) {
    else if (!strcmp(tag, RATE_WEIGHTS_TAG)) {
      if (nratecats < 0) 
        die("ERROR: NRATECATS must precede RATE_WEIGHTS in tree model file.\n");
      rate_weights = vec_new_from_file(f, nratecats);
    }
    else if (!strcmp(tag, SELECTION_TAG)) {
      str_readline(tmpstr, f);
      if (str_as_dbl(tmpstr, &selection)==1)
	die("ERROR: bad SELECTION line in tree model file\n");
      have_selection = 1;
    } 
    else if (!strcmp(tag, ALT_MODEL_TAG)) {
      break;
    }
    else {
      die("ERROR: unrecognized tag in model file (\"%s\").\n", 
	  tag);
    }
  }*/

    }

    return retval;
}

int extractResult(ListOfLists *curLOL, char **treeString, double *kappa){
    if (*treeString != NULL){
        *treeString = NULL;
    }
    *kappa = 0.0;
    if(curLOL != NULL){
        Matrix *subMatrix = NULL;
        List *treeResult = NULL;
        ListOfLists *rateMatrix = NULL;
        treeResult = lol_find_list(curLOL,"tree",CHAR_LIST);
        if(treeResult == NULL){
            fprintf(stderr, "Error: Couldn't find the tree!!!\n");
            exit(EXIT_FAILURE);
        }
    
        List *rawNames=NULL;
        int rawIndex=0, colIndex = 0;
    
        rateMatrix = lol_find_lol(curLOL,"rate.matrix");
        rawNames = lol_find_list(rateMatrix,"row.names",CHAR_LIST);
        double **array = NULL;
        array = (double**)smalloc((lst_size(rawNames)) * sizeof(double*));
        for (rawIndex=0; rawIndex< lst_size(rawNames); rawIndex++){
            List *rateRaw = NULL;
            char *rawName = (char*)lst_get_ptr(rawNames, rawIndex);
            rateRaw  = lol_find_list(rateMatrix,rawName,DBL_LIST);
            if(rateRaw == NULL){
                fprintf(stderr, "Error: Couldn't find the substitution raw at index %d !!!\n",rawIndex);
                exit(EXIT_FAILURE);
            }
            array[rawIndex] = (double*)smalloc(lst_size(rateRaw) * sizeof(double));
            for (colIndex = 0; colIndex < lst_size(rateRaw); colIndex++){
                array[rawIndex][colIndex] = lst_get_dbl(rateRaw, colIndex);
                //double rate = lst_get_dbl(rateRaw, colIndex);
                //fprintf(stdout,"%f\t",rate);
            }
            //fprintf(stdout,"\n");
        }
        subMatrix = mat_new_from_array(array,rawIndex,colIndex);
        for (int i = 0; i < colIndex; i++) {
          sfree(array[i]);
        }
        sfree(array);
        // I have my tree now
        for (int tip = 0; tip < lst_size(treeResult); tip++){
            *treeString = (char*)lst_get_ptr(treeResult, tip);
            if(!pars.quiet){
                fprintf(stdout,"%s\n",*treeString);
            }
        }
    
        // I need the Kappa corresponding to the tree
        *kappa = subMatrix->data[0][2] / subMatrix->data[1][2];
        mat_free(subMatrix);
    }

    return (curLOL!=NULL);
}

/* If from/to equal to -1 interface the entire block, otherwise the given 
 * frame. The interface exclude those species which are not in the tree, except
 * when the given tree is NULL, which contain all species.
 */

int interface_SS(MSA *msa, struct aln *alignedSeqs[],int from , int to, TreeNode *spTree) {

  int num_seq = 0, seen_index=0;
  char *name = NULL,*seq = NULL;
  int start,end,length,fullLength;
  char strand='+';

  for (num_seq=0; num_seq < msa->nseqs; num_seq++){
      name = strdup(msa->names[num_seq]);
      TreeNode *asked_node= (spTree == NULL) ? NULL : tr_get_node(spTree,name); 
      if ( spTree == NULL ||  asked_node != NULL){
          if ((from < 0) && (to < 0)){
             seq = strdup(msa->seqs[num_seq]);
             fullLength = msa->length;
             length = getSeqLength(msa->seqs[num_seq]);
          } else{
             int subLength = to-from;
             seq = malloc ((subLength+1)* sizeof(char));   // Space for length plus nul (subLength is +1 included)
             if (seq == NULL) {
               fprintf(stderr,"ERROR: Couldn't allocate memory of size %li"
                    " for %s\n",subLength,name);
               exit(EXIT_FAILURE);          // No memory
             }
             int pos=0;
             for(; (msa->seqs[num_seq][pos+from] != '\0') && (pos < subLength) ; ++pos){
                seq[pos] = msa->seqs[num_seq][pos+from];// same as ss_get_char_pos(msa, pos+from, num_seq, 0);
             }
             seq[pos] = '\0';
             //strncpy (d,&(msa->seqs[num_seq]+from),sizeof(d));   // Copy the characters
             fullLength = pos;
             length = getSeqLength(seq);
          }
          start = msa->idx_offset; // I don't think that I need this (edited on 22.06.2018) :: msa->ncats;
          strand = '+';
          alignedSeqs[seen_index]=createAlnEntry(name,seq,start,length,fullLength,strand);
          seen_index ++;
      } else {
          if(!pars.quiet){
              fprintf(stderr, "(%s, ", name);
          }
          if (name != NULL){
              free(name);
              name = NULL;
          }
      }
  }
  /*if (name != NULL){
      free(name);
      name = NULL;
  }
  if (seq != NULL){
      free(seq);
      seq = NULL;
  }*/
  if(!pars.quiet){
      if( seen_index != num_seq){
          fprintf(stderr,") were not in the tree!\n");
      }
  }

  alignedSeqs[seen_index] = NULL;

  return seen_index;
}
