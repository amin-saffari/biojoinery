AC_DEFUN([AX_LIB_PHASTLIB],
#
# Handle user hints
#
[AC_MSG_CHECKING([if PHAST is wanted])
AC_ARG_WITH([phastlib],
  AS_HELP_STRING([--with-phastlib],
                 [search for phast in DIR/include and DIR/lib]),
 [if test "$withval" != no ; then
   AC_MSG_RESULT([yes])
   if test -d "$withval" ; then
     PHAST_HOME="$withval"
   else
     PHAST_HOME="/usr/local"
     AC_MSG_WARN([Sorry, $withval does not exist, checking usual places])
   fi
 else
   AC_MSG_RESULT([no])
 fi],
 [AC_MSG_RESULT([yes])
  PHAST_HOME="/usr/local"
 ])

#
# Locate phastlibs, if wanted
#
if test -n "${PHAST_HOME}" ; then

	PHAST_CFLAGS="-I${PHAST_HOME}/include/ "
	PHAST_LDFLAGS="-L${PHAST_HOME}/lib/ -Wl,-rpath,${PHAST_HOME}/lib/ "

        PHAST_CFLAGS+="-I/home/khikho/code/extraLibs/CLAPACK-3.2.1/INCLUDE/ "
        PHAST_LDFLAGS+="-L/home/khikho/code/extraLibs/CLAPACK-3.2.1/F2CLIBS/ -Wl,-rpath,/home/khikho/code/extraLibs/CLAPACK-3.2.1/F2CLIBS/ "

        PHAST_LIBS="-lphast -ltmg -llapack -lblaswr -lf2c -lm "

        PHAST_OLD_LDFLAGS=$LDFLAGS
        PHAST_OLD_CFLAGS=$CFLAGS

        LDFLAGS="$LDFLAGS ${PHAST_LDFLAGS} ${PHAST_LIBS}"
        CFLAGS="$CFLAGS ${PHAST_CFLAGS}"
        AC_MSG_WARN([CFLGS = ${CFLAGS}])
        AC_MSG_WARN([LDFLGS = ${LDFLAGS}])
        
        AC_CHECK_FILE([${PHAST_HOME}/include/phylo_fit.h], [ac_f_phast_h=yes], [ac_f_phast_h=no])
        AC_MSG_WARN([ac_f_phast_h = $ac_f_phast_h])

        AC_CHECK_HEADERS([msa.h], [ac_cv_phast_h=yes], [ac_cv_phast_h=no])
        AC_CHECK_LIB([${PHAST_HOME}/lib/phast], [lapack], [ac_cv_libphast=yes], [ac_cv_libphast=no])
        # AC_LANG_RESTORE
        AC_MSG_WARN([CFLGS1 = ${CFLAGS}])

        AC_MSG_WARN([ac_cv_phast_h = $ac_cv_phast_h and ac_cv_libphast = $ac_cv_libphast])

        if test "$ac_cv_libphast" = "yes" && test "$ac_cv_phast_h" = "yes" ; then
                #
                # If both library and header were found, use them
                #
                AC_MSG_CHECKING([phastlib])
                AC_MSG_RESULT([ok])
		AC_SUBST(PHAST_CFLAGS)
        	AC_SUBST(PHAST_LDFLAGS)
		AC_SUBST(PHAST_LIBS)
                with_phast=yes
        else
                #
                # If either header or library was not found, revert and bomb
                #

                LDFLAGS="$PHAST_OLD_LDFLAGS"
                CFLAGS="$PHAST_OLD_CFLAGS"
     
                AC_MSG_CHECKING([phastlib])
                AC_MSG_RESULT([failed])
                AC_MSG_ERROR([specify a valid phast installation with --with-phastlib=DIR ])
        fi

fi
])

